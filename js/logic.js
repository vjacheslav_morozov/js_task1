var screenElement;
var baseNum = "", workNum = "", savedAction = "";

var onLoad = function() {
	var id = "calcScreen";
	screenElement = document.getElementById(id);
};

var show = function(message) {
	screenElement.innerHTML = message;
};

var onNum = function(num) {
	if (num || workNum) {
		workNum = workNum + "" + num;
		show(workNum);
	} else {
		show('0');
	}
};

var onMathAction = function(action) {
	if (!baseNum) {
		baseNum = workNum;
		workNum = "";
		savedAction = action;
		show(baseNum);
	} else {
		if(!workNum){
			savedAction = action;
		}else{
			onCompute();
			savedAction = action;
		}
	}
};

var onAdd = function(){
	onMathAction("add");
};

var onSub = function(){
	onMathAction("sub");
};

var onMul = function(){
	onMathAction("mul");
};

var onDiv = function(){
	onMathAction("div");
};

var onCompute = function() {
	if (savedAction == "add") {
		baseNum = baseNum * 1 + workNum * 1;
	}
	if (savedAction == "mul") {
		baseNum = baseNum * workNum;
	}
	if (savedAction == "sub") {
		baseNum = baseNum * 1 - workNum * 1;
	}
	if (savedAction == "div") {
		baseNum = baseNum / workNum;
	}
	if (!savedAction && workNum) {
		baseNum = workNum;
	}
	workNum = "";
	savedAction = "";
	show(baseNum);
};

var onInvert = function() {
	if (!workNum) {
		workNum = baseNum;
		baseNum = "";
	}
	workNum = workNum * (-1);
	show(workNum);
};

var onSqrt = function() {
	if (!workNum) {
		workNum = baseNum;
		baseNum = "";
	}
	workNum = Math.sqrt(workNum);
	show(workNum);
};

var onSoftResset = function() {
	workNum = "";
	show("0");
};

var onReset = function() {
	baseNum = "";
	workNum = "";
	show("0");
};